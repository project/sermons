<?php

/**
 * Implementation of hook_content_default_fields().
 */
function sermons_content_default_fields() {
  module_load_include('inc', 'sermons', 'sermons.defaults');
  $args = func_get_args();
  return call_user_func_array('_sermons_content_default_fields', $args);
}

/**
 * Implementation of hook_context_default_contexts().
 */
function sermons_context_default_contexts() {
  module_load_include('inc', 'sermons', 'sermons.defaults');
  $args = func_get_args();
  return call_user_func_array('_sermons_context_default_contexts', $args);
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function sermons_imagecache_default_presets() {
  module_load_include('inc', 'sermons', 'sermons.defaults');
  $args = func_get_args();
  return call_user_func_array('_sermons_imagecache_default_presets', $args);
}

/**
 * Implementation of hook_node_info().
 */
function sermons_node_info() {
  module_load_include('inc', 'sermons', 'sermons.features.node');
  $args = func_get_args();
  return call_user_func_array('_sermons_node_info', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function sermons_views_default_views() {
  module_load_include('inc', 'sermons', 'sermons.features.views');
  $args = func_get_args();
  return call_user_func_array('_sermons_views_default_views', $args);
}
