<?php

/**
 * Helper to implementation of hook_views_default_views().
 */
function _sermons_views_default_views() {
  $views = array();

  // Exported view: Sermons
  $view = new view;
  $view->name = 'Sermons';
  $view->description = 'Varios list of sermons to be used in different ways.';
  $view->tag = 'sermons';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Default', 'default');
  $handler->override_option('fields', array(
    'field_sermon_graphic_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'label_type' => 'none',
      'format' => 'sermon_thumbnail_linked',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'field_sermon_graphic_fid',
      'table' => 'node_data_field_sermon_graphic',
      'field' => 'field_sermon_graphic_fid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_time_value' => array(
      'label' => 'Date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'day_am_pm',
      'multiple' => array(
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'group' => TRUE,
      ),
      'repeat' => array(
        'show_repeat_rule' => '',
      ),
      'fromto' => array(
        'fromto' => 'both',
      ),
      'exclude' => 0,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_book_bible_value' => array(
      'id' => 'field_sermon_book_bible_value',
      'table' => 'node_data_field_sermon_book_bible',
      'field' => 'field_sermon_book_bible_value',
    ),
    'field_sermon_chapter_value' => array(
      'id' => 'field_sermon_chapter_value',
      'table' => 'node_data_field_sermon_chapter',
      'field' => 'field_sermon_chapter_value',
    ),
    'field_sermon_audio_fid' => array(
      'id' => 'field_sermon_audio_fid',
      'table' => 'node_data_field_sermon_audio',
      'field' => 'field_sermon_audio_fid',
    ),
    'field_sermon_notes_fid' => array(
      'id' => 'field_sermon_notes_fid',
      'table' => 'node_data_field_sermon_notes',
      'field' => 'field_sermon_notes_fid',
    ),
  ));
  $handler->override_option('sorts', array(
    'field_sermon_time_value' => array(
      'order' => 'DESC',
      'delta' => -1,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'field_sermon_series_nid' => array(
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => 'Sermons from %1',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'node',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'field_sermon_series_nid',
      'table' => 'node_data_field_sermon_series',
      'field' => 'field_sermon_series_nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
      ),
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'sermon_series' => 'sermon_series',
        'webform' => 0,
        'sermon' => 0,
        'event' => 0,
        'images' => 0,
        'ministry' => 0,
        'page' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sermon' => 'sermon',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Sermons');
  $handler->override_option('header', '<p>We\'re glad to share these sermons with you. If you would like to discuss something from these sermons in greater detail, please feel free to contact us through the <a href="/contact">contact form</a>. We would be glad to hear from you.</p>');
  $handler->override_option('header_format', '2');
  $handler->override_option('header_empty', 1);
  $handler->override_option('use_ajax', TRUE);
  $handler->override_option('use_pager', '1');
  $handler->override_option('row_plugin', 'node');
  $handler->override_option('row_options', array(
    'relationship' => 'none',
    'build_mode' => 'teaser',
    'links' => 1,
    'comments' => 0,
  ));
  $handler = $view->new_display('page', 'Desc. Table', 'page_1');
  $handler->override_option('fields', array(
    'field_sermon_graphic_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'label_type' => 'none',
      'format' => 'sermon_thumbnail_linked',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'field_sermon_graphic_fid',
      'table' => 'node_data_field_sermon_graphic',
      'field' => 'field_sermon_graphic_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_time_value' => array(
      'label' => 'Date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'day_am_pm',
      'multiple' => array(
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'group' => TRUE,
      ),
      'repeat' => array(
        'show_repeat_rule' => '',
      ),
      'fromto' => array(
        'fromto' => 'both',
      ),
      'exclude' => 0,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_book_bible_value' => array(
      'label' => 'Book of the Bible',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_book_bible_value',
      'table' => 'node_data_field_sermon_book_bible',
      'field' => 'field_sermon_book_bible_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_chapter_value' => array(
      'id' => 'field_sermon_chapter_value',
      'table' => 'node_data_field_sermon_chapter',
      'field' => 'field_sermon_chapter_value',
    ),
    'field_sermon_audio_fid' => array(
      'label' => 'Listen',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'swftools_no_file',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_audio_fid',
      'table' => 'node_data_field_sermon_audio',
      'field' => 'field_sermon_audio_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_notes_fid' => array(
      'label' => 'Notes',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_notes_fid',
      'table' => 'node_data_field_sermon_notes',
      'field' => 'field_sermon_notes_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'field_sermon_graphic_fid' => 'field_sermon_graphic_fid',
      'title' => 'title',
      'field_sermon_time_value' => 'title',
      'field_sermon_book_bible_value' => 'field_sermon_book_bible_value',
      'field_sermon_chapter_value' => 'field_sermon_book_bible_value',
      'field_sermon_audio_fid' => 'field_sermon_audio_fid',
      'field_sermon_notes_fid' => 'field_sermon_notes_fid',
    ),
    'info' => array(
      'field_sermon_graphic_fid' => array(
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 0,
        'separator' => '<br />',
      ),
      'field_sermon_time_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_book_bible_value' => array(
        'sortable' => 0,
        'separator' => '&nbsp;',
      ),
      'field_sermon_chapter_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_audio_fid' => array(
        'separator' => '',
      ),
      'field_sermon_notes_fid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler->override_option('row_plugin', 'fields');
  $handler->override_option('row_options', array(
    'inline' => array(),
    'separator' => '',
    'hide_empty' => 0,
  ));
  $handler->override_option('path', 'sermons');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => 'Latest',
    'description' => 'Latest Sermons',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'normal',
    'title' => 'Sermons',
    'description' => 'Latest Sermons',
    'weight' => '0',
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('fields', array(
    'title' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_time_value' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'short',
      'multiple' => array(
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'group' => TRUE,
      ),
      'repeat' => array(
        'show_repeat_rule' => '',
      ),
      'fromto' => array(
        'fromto' => 'both',
      ),
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Latest Sermons');
  $handler->override_option('header', '');
  $handler->override_option('header_format', '1');
  $handler->override_option('header_empty', 0);
  $handler->override_option('use_ajax', FALSE);
  $handler->override_option('items_per_page', 5);
  $handler->override_option('use_pager', FALSE);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 0,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'field_sermon_time_value' => 'field_sermon_time_value',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_time_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler->override_option('row_plugin', 'fields');
  $handler->override_option('row_options', array(
    'inline' => array(
      'title' => 'title',
    ),
    'separator' => '',
  ));
  $handler->override_option('block_description', 'Latest Sermons');
  $handler->override_option('block_caching', -1);
  $handler = $view->new_display('page', 'Asc. Table', 'page_2');
  $handler->override_option('sorts', array(
    'field_sermon_time_value' => array(
      'order' => 'ASC',
      'delta' => -1,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Use default',
      ),
    ),
  ));
  $handler->override_option('use_pager', 'mini');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'field_sermon_time_value' => 'field_sermon_time_value',
      'field_sermon_graphic_fid' => 'field_sermon_graphic_fid',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_time_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_graphic_fid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler->override_option('path', 'sermon-series/%');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Sermons By Date', 'page_3');
  $handler->override_option('fields', array(
    'field_sermon_graphic_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'label_type' => 'none',
      'format' => 'sermon_thumbnail_linked',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'field_sermon_graphic_fid',
      'table' => 'node_data_field_sermon_graphic',
      'field' => 'field_sermon_graphic_fid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_speaker_uid' => array(
      'label' => 'Speaker',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_speaker_uid',
      'table' => 'node_data_field_sermon_speaker',
      'field' => 'field_sermon_speaker_uid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_time_value' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'year_month',
      'multiple' => array(
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'group' => TRUE,
      ),
      'repeat' => array(
        'show_repeat_rule' => '',
      ),
      'fromto' => array(
        'fromto' => 'both',
      ),
      'exclude' => 1,
      'id' => 'field_sermon_time_value',
      'table' => 'node_data_field_sermon_time',
      'field' => 'field_sermon_time_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_book_bible_value' => array(
      'label' => 'Passage',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_book_bible_value',
      'table' => 'node_data_field_sermon_book_bible',
      'field' => 'field_sermon_book_bible_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_chapter_value' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_chapter_value',
      'table' => 'node_data_field_sermon_chapter',
      'field' => 'field_sermon_chapter_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_series_nid' => array(
      'label' => 'Sermon Series',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_series_nid',
      'table' => 'node_data_field_sermon_series',
      'field' => 'field_sermon_series_nid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'Sermons By Date');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => 'field_sermon_time_value',
    'override' => 0,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'field_sermon_graphic_fid' => 'field_sermon_graphic_fid',
      'title' => 'title',
      'field_sermon_speaker_uid' => 'field_sermon_speaker_uid',
      'field_sermon_time_value' => 'field_sermon_time_value',
      'field_sermon_book_bible_value' => 'field_sermon_book_bible_value',
      'field_sermon_chapter_value' => 'field_sermon_chapter_value',
      'field_sermon_series_nid' => 'field_sermon_series_nid',
    ),
    'info' => array(
      'field_sermon_graphic_fid' => array(
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_speaker_uid' => array(
        'separator' => '<br />',
      ),
      'field_sermon_time_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_book_bible_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_chapter_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_series_nid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler->override_option('path', 'sermons/by-date');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Sermons By Date',
    'description' => 'Sermons By Date',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Sermon Search', 'page_4');
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sermon' => 'sermon',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'field_sermon_book_bible_value_many_to_one' => array(
      'operator' => 'or',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'field_sermon_book_bible_value_many_to_one_op',
        'identifier' => 'field_sermon_book_bible_value_many_to_one',
        'label' => 'Book of the Bible',
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'field_sermon_book_bible_value_many_to_one',
      'table' => 'node_data_field_sermon_book_bible',
      'field' => 'field_sermon_book_bible_value_many_to_one',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'field_sermon_chapter_value' => array(
      'operator' => 'contains',
      'value' => '',
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'field_sermon_chapter_value_op',
        'identifier' => 'field_sermon_chapter_value',
        'label' => 'Chapter',
        'optional' => 1,
        'remember' => 0,
      ),
      'case' => 0,
      'id' => 'field_sermon_chapter_value',
      'table' => 'node_data_field_sermon_chapter',
      'field' => 'field_sermon_chapter_value',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
    'field_sermon_series_nid' => array(
      'operator' => 'or',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'field_sermon_series_nid_op',
        'identifier' => 'field_sermon_series_nid',
        'label' => 'Sermon Series',
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'field_sermon_series_nid',
      'table' => 'node_data_field_sermon_series',
      'field' => 'field_sermon_series_nid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
    'field_sermon_speaker_uid' => array(
      'operator' => 'or',
      'value' => array(),
      'group' => '0',
      'exposed' => TRUE,
      'expose' => array(
        'use_operator' => 0,
        'operator' => 'field_sermon_speaker_uid_op',
        'identifier' => 'field_sermon_speaker_uid',
        'label' => 'Speaker',
        'optional' => 1,
        'single' => 1,
        'remember' => 0,
        'reduce' => 0,
      ),
      'id' => 'field_sermon_speaker_uid',
      'table' => 'node_data_field_sermon_speaker',
      'field' => 'field_sermon_speaker_uid',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
      'reduce_duplicates' => 0,
    ),
  ));
  $handler->override_option('title', 'Sermon Search');
  $handler->override_option('path', 'sermons/search');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Search',
    'description' => 'Search the sermons',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  $handler = $view->new_display('page', 'Chronological', 'page_5');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'field_sermon_time_value' => 'title',
      'field_sermon_graphic_fid' => 'field_sermon_graphic_fid',
      'field_sermon_book_bible_value' => 'field_sermon_book_bible_value',
      'field_sermon_chapter_value' => 'field_sermon_book_bible_value',
      'field_sermon_audio_fid' => 'field_sermon_audio_fid',
      'field_sermon_notes_fid' => 'field_sermon_notes_fid',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '<br />',
      ),
      'field_sermon_time_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_graphic_fid' => array(
        'separator' => '',
      ),
      'field_sermon_book_bible_value' => array(
        'sortable' => 0,
        'separator' => '&nbsp;',
      ),
      'field_sermon_chapter_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_sermon_audio_fid' => array(
        'separator' => '',
      ),
      'field_sermon_notes_fid' => array(
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler->override_option('path', 'sermons/chronological');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'Latest',
    'description' => 'Latest Sermons',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => '0',
  ));

  $views[$view->name] = $view;

  // Exported view: sermon_series
  $view = new view;
  $view->name = 'sermon_series';
  $view->description = 'Sermon Series Lists';
  $view->tag = 'sermons series';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'field_sermon_graphic_fid' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'none',
      'format' => 'image_nodelink',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_sermon_graphic_fid',
      'table' => 'node_data_field_sermon_graphic',
      'field' => 'field_sermon_graphic_fid',
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'created' => array(
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
    ),
  ));
  $handler->override_option('filters', array(
    'status' => array(
      'operator' => '=',
      'value' => '1',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status',
      'table' => 'node',
      'field' => 'status',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sermon_series' => 'sermon_series',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->override_option('sorts', array(
    'created' => array(
      'order' => 'ASC',
      'granularity' => 'day',
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'override' => array(
        'button' => 'Use default',
      ),
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('title', 'The Current Sermon Series Graphic');
  $handler->override_option('items_per_page', 1);
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', '4');

  $views[$view->name] = $view;

  return $views;
}
