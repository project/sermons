<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _sermons_content_default_fields() {
  $fields = array();

  // Exported field: field_sermon_audio
  $fields[] = array(
    'field_name' => 'field_sermon_audio',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'swftools_no_file',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'swftools',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'url_plain',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '1',
    'widget' => array(
      'file_extensions' => 'mp3 aac',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'label' => 'Sermon Audio',
      'weight' => '-2',
      'description' => '',
      'type' => 'filefield_widget',
      'module' => 'filefield',
    ),
  );

  // Exported field: field_sermon_book_bible
  $fields[] = array(
    'field_name' => 'field_sermon_book_bible',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '3',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '
gen|Genesis
ex|Exodus
lev|Leviticus
num|Numbers
deut|Deuteronomy
josh|Joshua
judg|Judges
ruth|Ruth
1sam|1 Samuel
2sam|2 Samuel
1king|1 Kings
2king|2 Kings
1chr|1 Chronicles
2chr|2 Chronicles
ezra|Ezra
neh|Nehemiah
esth|Esther
job|Job
psalm|Psalm
prov|Proverbs
eccl|Ecclesiastes
solomon|Song of Solomon
isa|Isaiah
jer|Jeremiah
lam|Lamentations
eze|Ezekiel
dan|Daniel
hosea|Hosea
joel|Joel
amos|Amos
obad|Obadiah
jonah|Jonah
micah|Micah
nahum|Nahum
habak|Habakkuk
zeph|Zephaniah
hag|Haggai
zech|Zechariah
mal|Malachi

matt|Matthew
mark|Mark
luke|Luke
john|John
acts|Acts
rom|Romans
1cor|1 Corinthians
2cor|2 Corinthians
gal|Galatians
eph|Ephesians
phil|Philippians
col|Colossians
1thes|1 Thessalonians
2thes|2 Thessalonians
1tim|1 Timothy
2tim|2 Timothy
titus|Titus
phil|Philemon
heb|Hebrews
jam|James
1pete|1 Peter
2pete|2 Peter
1joh|1 John
2joh|2 John
3joh|3 John
jude|Jude
rev|Revelation',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Book of the Bible',
      'weight' => '3',
      'description' => '',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_sermon_chapter
  $fields[] = array(
    'field_name' => 'field_sermon_chapter',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '10',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '10',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_sermon_chapter][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Chapter',
      'weight' => '4',
      'description' => 'Enter the chapters. If one chapter simply enter the number. If multiple chapters enter as in the following examples. Ex. 5-7 or 5-7,9',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_sermon_graphic
  $fields[] = array(
    'field_name' => 'field_sermon_graphic',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '5',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'sermon_image_linked',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'front_page_image_default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'image_plain',
        'exclude' => 1,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'png gif jpg jpeg',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => '0',
      'min_resolution' => '0',
      'alt' => '',
      'custom_alt' => 0,
      'title' => '',
      'custom_title' => 0,
      'title_type' => 'textfield',
      'default_image' => NULL,
      'use_default_image' => 0,
      'label' => 'Graphic',
      'weight' => '5',
      'description' => 'You may provide a graphic here that will represent the sermon visually.',
      'type' => 'imagefield_widget',
      'module' => 'imagefield',
    ),
  );

  // Exported field: field_sermon_notes
  $fields[] = array(
    'field_name' => 'field_sermon_notes',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '2',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '1',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'txt pdf doc odt',
      'file_path' => 'sermon-notes',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'label' => 'Sermon Notes',
      'weight' => '2',
      'description' => '',
      'type' => 'filefield_widget',
      'module' => 'filefield',
    ),
  );

  // Exported field: field_sermon_series
  $fields[] = array(
    'field_name' => 'field_sermon_series',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'plain',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'sermon_series' => 'sermon_series',
      'forum' => 0,
      'page' => 0,
      'profile' => 0,
      'sermon' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Sermon Series',
      'weight' => '-3',
      'description' => 'If this sermon is part of a series, please select it from the list.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_sermon_speaker
  $fields[] = array(
    'field_name' => 'field_sermon_speaker',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'userreference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'userreference',
    'active' => '1',
    'referenceable_roles' => array(
      '2' => 0,
      '3' => 0,
    ),
    'referenceable_status' => array(
      '1' => 1,
      '0' => 0,
    ),
    'advanced_view' => '',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => 60,
      'reverse_link' => 1,
      'default_value' => array(
        '0' => array(
          'uid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Speaker',
      'weight' => '-4',
      'description' => '',
      'type' => 'userreference_select',
      'module' => 'userreference',
    ),
  );

  // Exported field: field_sermon_time
  $fields[] = array(
    'field_name' => 'field_sermon_time',
    'type_name' => 'sermon',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'medium',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'datetime',
    'required' => '1',
    'multiple' => '0',
    'module' => 'date',
    'active' => '1',
    'granularity' => array(
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
      'hour' => 'hour',
      'minute' => 'minute',
    ),
    'timezone_db' => 'UTC',
    'tz_handling' => 'site',
    'todate' => '',
    'repeat' => 0,
    'repeat_collapsed' => '',
    'default_format' => 'medium',
    'widget' => array(
      'default_value' => 'now',
      'default_value_code' => '',
      'default_value2' => 'same',
      'default_value_code2' => '',
      'input_format' => 'm/d/Y - g:ia',
      'input_format_custom' => '',
      'increment' => '1',
      'text_parts' => array(),
      'year_range' => '-3:+3',
      'label_position' => 'above',
      'label' => 'Sermon Time',
      'weight' => '-1',
      'description' => '',
      'type' => 'date_popup',
      'module' => 'date',
    ),
  );

  // Exported field: field_sermon_book_bible
  $fields[] = array(
    'field_name' => 'field_sermon_book_bible',
    'type_name' => 'sermon_series',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '
gen|Genesis
ex|Exodus
lev|Leviticus
num|Numbers
deut|Deuteronomy
josh|Joshua
judg|Judges
ruth|Ruth
1sam|1 Samuel
2sam|2 Samuel
1king|1 Kings
2king|2 Kings
1chr|1 Chronicles
2chr|2 Chronicles
ezra|Ezra
neh|Nehemiah
esth|Esther
job|Job
psalm|Psalm
prov|Proverbs
eccl|Ecclesiastes
solomon|Song of Solomon
isa|Isaiah
jer|Jeremiah
lam|Lamentations
eze|Ezekiel
dan|Daniel
hosea|Hosea
joel|Joel
amos|Amos
obad|Obadiah
jonah|Jonah
micah|Micah
nahum|Nahum
habak|Habakkuk
zeph|Zephaniah
hag|Haggai
zech|Zechariah
mal|Malachi

matt|Matthew
mark|Mark
luke|Luke
john|John
acts|Acts
rom|Romans
1cor|1 Corinthians
2cor|2 Corinthians
gal|Galatians
eph|Ephesians
phil|Philippians
col|Colossians
1thes|1 Thessalonians
2thes|2 Thessalonians
1tim|1 Timothy
2tim|2 Timothy
titus|Titus
phil|Philemon
heb|Hebrews
jam|James
1pete|1 Peter
2pete|2 Peter
1joh|1 John
2joh|2 John
3joh|3 John
jude|Jude
rev|Revelation',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Book of the Bible',
      'weight' => '-3',
      'description' => 'Select the primary book of the Bible.',
      'type' => 'optionwidgets_select',
      'module' => 'optionwidgets',
    ),
  );

  // Exported field: field_sermon_chapter
  $fields[] = array(
    'field_name' => 'field_sermon_chapter',
    'type_name' => 'sermon_series',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '10',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '10',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_sermon_chapter][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Chapter',
      'weight' => '-2',
      'description' => 'Enter the chapters. If one chapter simply enter the number. If multiple chapters enter as in the following examples. Ex. 5-7 or 5-7,9',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_sermon_graphic
  $fields[] = array(
    'field_name' => 'field_sermon_graphic',
    'type_name' => 'sermon_series',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'image_plain',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'filefield',
    'required' => '0',
    'multiple' => '0',
    'module' => 'filefield',
    'active' => '1',
    'list_field' => '0',
    'list_default' => 1,
    'description_field' => '0',
    'widget' => array(
      'file_extensions' => 'png gif jpg jpeg',
      'file_path' => '',
      'progress_indicator' => 'bar',
      'max_filesize_per_file' => '',
      'max_filesize_per_node' => '',
      'max_resolution' => '0',
      'min_resolution' => '0',
      'alt' => '',
      'custom_alt' => 0,
      'title' => '',
      'custom_title' => 0,
      'title_type' => 'textfield',
      'default_image' => NULL,
      'use_default_image' => 0,
      'label' => 'Graphic',
      'weight' => '-4',
      'description' => '',
      'type' => 'imagefield_widget',
      'module' => 'imagefield',
    ),
  );

  // Translatables
  array(
    t('Book of the Bible'),
    t('Chapter'),
    t('Graphic'),
    t('Sermon Audio'),
    t('Sermon Notes'),
    t('Sermon Series'),
    t('Sermon Time'),
    t('Speaker'),
  );

  return $fields;
}

/**
 * Helper to implementation of hook_context_default_contexts().
 */
function _sermons_context_default_contexts() {
  $items = array();

  $items[] = array(
    'namespace' => 'church',
    'attribute' => 'sermons',
    'value' => 'sermons',
    'description' => 'Sermon Elements',
    'node' => array(
      '0' => 'sermon',
      '1' => 'sermon_series',
    ),
    'path' => array(
      'sermons/* sermons sermon sermon/* sermon_series sermon_series/*' => 'sermons/* sermons sermon sermon/* sermon_series sermon_series/*',
    ),
    'menu_trail' => array(
      'sermons' => 'sermons',
    ),
    'block' => array(),
  );
  return $items;
}

/**
 * Helper to implementation of hook_imagecache_default_presets().
 */
function _sermons_imagecache_default_presets() {
  $items = array(
    'front_page_image' => array(
      'presetname' => 'front_page_image',
      'actions' => array(
        '0' => array(
          'actionid' => '1',
          'presetid' => '1',
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '350',
            'height' => '150',
            'upscale' => 1,
          ),
        ),
      ),
    ),
    'sermon_image' => array(
      'presetname' => 'sermon_image',
      'actions' => array(
        '0' => array(
          'actionid' => '3',
          'presetid' => '3',
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '250px',
            'height' => '250px',
          ),
        ),
      ),
    ),
    'sermon_thumbnail' => array(
      'presetname' => 'sermon_thumbnail',
      'actions' => array(
        '0' => array(
          'actionid' => '2',
          'presetid' => '2',
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale_and_crop',
          'data' => array(
            'width' => '25px',
            'height' => '25px',
          ),
        ),
      ),
    ),
  );
  return $items;
}
