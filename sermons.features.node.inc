<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _sermons_node_info() {
  $items = array(
    'sermon' => array(
      'name' => t('Sermon'),
      'module' => 'features',
      'description' => t('A sermon is all the information related to a sermon. It may include audio of the sermon, who preached it, when it was preached, the date and time it was preached along with an short description of the sermon, the passage that was spoken from and tags to help group the sermon with other similar sermons.'),
      'has_title' => '1',
      'title_label' => t('Sermon Title'),
      'has_body' => '1',
      'body_label' => t('Sermon Abstract'),
    ),
    'sermon_series' => array(
      'name' => t('Sermon Series'),
      'module' => 'features',
      'description' => t('A Sermon Series consist of a collection of sermons preached consecutively on a specific topic or passage of the Bible. They may be the greater part of a year of Sundays or only a couple Sundays.'),
      'has_title' => '1',
      'title_label' => t('Series Title'),
      'has_body' => '1',
      'body_label' => t('Series Abstract'),
    ),
  );
  return $items;
}
